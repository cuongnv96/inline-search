const SOURCE = "word_source";
const LAST_TRANS_RESULT = "last_trans_result";

// actionId
var ACT_TRANSLATE 		 = "translate";
var ACT_TRANSLATE_RESULT = "translateResult";

var resultWaiting;

$(document).ready(function() {
	initApp();
});

function initApp() {
	chrome.runtime.onMessage.addListener(handleResponse);

	$("#info-split").hide();

	var sourceView = $("#txt-source");
	sourceView.focus();	
	sourceView.keydown(function() {
		if (event.keyCode == 13) {
			var source = sourceView.val().trim();
			sourceView.val(source);

			doTranslate(source);
		}
	});

	$("#lang-switch").click(function() {
		var leftSelect = $("#sl-from")[0].selectedIndex;
		var rightSelect = $("#sl-to")[0].selectedIndex;
		$("#sl-from")[0].selectedIndex = rightSelect;
		$("#sl-to")[0].selectedIndex = leftSelect;
	});

	storage.get(SOURCE, (source) => {
		resultWaiting = source;
		sourceView.text(resultWaiting);
	});

	storage.get(LAST_TRANS_RESULT, (request) => {
		updateResult(request);
	});
};

function doTranslate(source) {
	$("#txt-source").prop("disabled", true);
	resultWaiting = source;

	storage.set(SOURCE, resultWaiting);

	chrome.runtime.sendMessage({
		actionId: ACT_TRANSLATE,
		data: source
	});
};

function handleResponse(request, response, sendResponse) {
	if (request.actionId === ACT_TRANSLATE_RESULT) {
		if (request.resultId == resultWaiting) {
			updateResult(request);
		}
	}
};

function updateResult(request) {
	storage.set(LAST_TRANS_RESULT, request);

	$("#txt-source").prop("disabled", false);
	$("#txt-source").focus();
	$("#txt-source").select();
	console.log(request);

	// info
	safeRun(() => {
		// $("#trans-info").html(request.result);
		writeResult(request.result);
		$("#info-split").show();
	}, () => {
		$("#trans-info").html("");
		$("#info-split").hide();
	});
};

function safeRun(job, fail) {
	try {
		job();
	} catch (e) {
		console.log(e);
		if (fail != null) {
			fail();
		}
	}
};

function writeResult(htmlSource) {
	var doc = document.getElementById('html-result').contentWindow.document;
	doc.open();
	doc.write(htmlSource);
	doc.close();
}