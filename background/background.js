var TRANS_URL = "https://www.google.com/search?q={keyword}";
// var TRANS_URL = "https://www.google.com/search?source=hp&ei=IT9BXOjlIceH9QOk9ZLIAQ&q={keyword}&oq={keyword}&gs_l=mobile-gws-wiz-hp.3..46i39i275j35i39l2j0i67j0.1353.1846..2260...1.0..0.173.654.0j5......0....1.......5.A5aAaIKd0uU";
var USER_AGENT = "Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36";

// actionId
var ACT_TRANSLATE = "translate";
var ACT_TRANSLATE_RESULT = "translateResult";

chrome.runtime.onMessage.addListener(handleRequest);

function handleRequest(request, response, sendResponse) {
    if (request.actionId === ACT_TRANSLATE) {
        translateText(request, response);
    }
};

function translateText(request, response) {
    $.ajax({
        beforeSend: function (jqXHR, settings) {
            jqXHR.setRequestHeader('User-Agent', USER_AGENT);
        },
        url: TRANS_URL.replace("{keyword}", request.data),
        dataType: 'html',
        success: function (data) {
            chrome.runtime.sendMessage({
                actionId: ACT_TRANSLATE_RESULT,
                resultId: request.data,
                result: data
            });
        },
        error: function (err) {
            chrome.runtime.sendMessage({
                actionId: ACT_TRANSLATE_RESULT,
                resultId: request.data,
                result: null
            });
        }
    });
};